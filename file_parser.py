import argparse
import string

# Script for getting statistics of word occurrences.
# Put file using file's path argument as --fp
# Example: python file_parser.py --fp=test.txt

parser = argparse.ArgumentParser(description="Put file's path")
parser.add_argument('--fp', type=str, help="file's path")
args = parser.parse_args()
file_path = args.fp
alphabet = list(string.ascii_letters)

# Read file data and remove all characters which are not letter or space
with open(file_path) as data:
    text = u"{}".format(data.read())
    pure_text = ''
    for i, char in enumerate(text):
        if char in alphabet or char == ' ':
            pure_text += char
    words = ''.join(pure_text).split(' ')

# Get only unique words
unique_words = set(words)

# Remove empty word, which can be when text contains multiple spaces
if '' in unique_words:
    unique_words.remove('')

# Get list of tuples like (word, count)
words_with_count = [(word, words.count(word)) for word in unique_words]

# Get all counts
all_counts = set(tup[1] for tup in words_with_count)

# Get list of lists with tuples grouped by count
sorted_list_by_count = []

for i, count in enumerate(all_counts):
    sorted_list_by_count.append(list(filter(lambda tup: tup[1] == count, words_with_count)))

# Sort in descending order of counts
sorted_list_by_count.reverse()

# Sort every group in alphabetic order of words in tuples and print on a new line in format: <word>: <frequency>
for i, single_group in enumerate(sorted_list_by_count):
    single_group.sort(key=lambda tup: tup[0][0].lower())
    for i, tup in enumerate(single_group):
        word = tup[0]
        frequency = tup[1]
        print(f'<{word}>: <{frequency}>')
